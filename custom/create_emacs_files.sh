#!/bin/bash

EMACS_DIR=$HOME"/.emacs.d"
EMACS_CUSTOM_DIR=$EMACS_DIR"/custom"
EMACS_CUSTOM_SETUPS_DIR=$EMACS_CUSTOM_DIR"/setup"

FILES_ARRAY=( 	editing convenience files text data external communication programming \
			 	applications development environment faces help multimedia local helm )

clear; clear;

echo "Creating directories..."
mkdir --parents $EMACS_CUSTOM_SETUPS_DIR

echo "Creating files"
for fileID in ${FILES_ARRAY[@]}
do
	file=$EMACS_CUSTOM_SETUPS_DIR"/setup-"$fileID".el"
	echo "Creating file with path $file"
	touch $file
done
