(message "Loading all setup modules")

;;; To load all the modules
(add-to-list 'load-path "~/.emacs.d/custom/setup/")

(require 'setup-editing)
(require 'setup-convenience)

(require 'setup-files)
(require 'setup-environment)
(require 'setup-help)

(require 'setup-applications)
(require 'setup-programming)
(require 'setup-faces-and-ui)


;; (require 'setup-communication)
;; (require 'setup-data)
;; (require 'setup-development)
;; (require 'setup-external)
;; (require 'setup-text)
;; (require 'setup-local)
