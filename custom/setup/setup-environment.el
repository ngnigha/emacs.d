(message "Setting environment")

;;;
(setq inhibit-startup-screen t)

;;;
(setq savehist-additional-variables '(search ring regexp-search-ring)
      savehist-autosave-interval    1)

;;;
(winner-mode 1)
(column-number-mode 1)

;;; Pacakges
;;; Nyan-mode: show percentage of the file traversed
(require 'nyan-mode)
;; (case window-system ((x w32) (nyan-mode)))

;;; Golden-ratio: automatic resizing of the opened frame
(require 'golden-ratio)
(add-to-list 'golden-ratio-exclude-modes "ediff-mode")
(add-to-list 'golden-ratio-exclude-modes "helm-mode")
(add-to-list 'golden-ratio-inhibit-functions 'pl/helm-alive-p)

(defun pl/helm-alive-p ()
  (if (boundp 'helm-alive-p)
      (symbol-value 'helm-alive-p)))

;; do not enable golden-raio in thses modes
(setq golden-ratio-exclude-modes '("ediff-mode"
                                   "gud-mode"
                                   "gdb-locals-mode"
                                   "gdb-registers-mode"
                                   "gdb-breakpoints-mode"
                                   "gdb-threads-mode"
                                   "gdb-frames-mode"
                                   "gdb-inferior-io-mode"
                                   "gud-mode"
                                   "gdb-inferior-io-mode"
                                   "gdb-disassembly-mode"
                                   "gdb-memory-mode"
                                   "magit-log-mode"
                                   "magit-reflog-mode"
                                   "magit-status-mode"
                                   "IELM"
                                   "eshell-mode"
;;                                   "dired-mode"
                                   )
      )
(golden-ratio-mode)


(provide 'setup-environment)
