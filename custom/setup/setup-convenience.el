(message "Setting up convenience")

;;; windows and frames navigation
(global-set-key (kbd "C-x <up>") 'windmove-up)
(global-set-key (kbd "C-x <down>") 'windmove-down)
(global-set-key (kbd "C-x <left>") 'windmove-left)
(global-set-key (kbd "C-x <right>") 'windmove-right)

;;; update changes made on file to the current buffer
(global-auto-revert-mode)

;;; word completion expansion
(global-set-key (kbd "M-/") 'hippie-expand)
(setq
    hippie-expand-try-functions-list
    '(try-expand-dabbrev ;; Try to expand word \"dynamically\", searching the current buffer.
         try-expand-dabbrev-all-buffers ;; Try to expand word \"dynamically\", searching all other buffers.
         try-expand-dabbrev-from-kill ;; Try to expand word \"dynamically\", searching the kill ring.
         try-complete-file-name-partially ;; Try to complete text as a file name, as many characters as unique.
         try-complete-file-name ;; Try to complete text as a file name.
         try-expand-all-abbrevs ;; Try to expand word before point according to all abbrev tables.
         try-expand-list ;; Try to complete the current line to an entire line in the buffer.
         try-expand-line ;; Try to complete the current line to an entire line in the buffer.
         try-complete-lisp-symbol-partially ;; Try to complete as an Emacs Lisp symbol, as many characters as unique.
         try-complete-lisp-symbol ;; Try to complete word as an Emacs Lisp symbol.
         )
    )

;;; Highlight current line
(global-hl-line-mode)

;;; ibuffer: always display ibuffer in another window
(setq ibuffer-use-other-window t)

;;; Linum: enable linum only in programming modes
(add-hook 'prog-mode-hook 'linum-mode)

;;; whenever you create useless whitespace, the whitespace is highlighted
(add-hook 'prog-mode-hook (lambda () (interactive) (setq show-trailing-whitespace 1)))

;;; activate whitespace-mode to view all whitespace characters
(global-set-key (kbd "C-c w") 'whitespace-mode)

;;; easier window navigation
(windmove-default-keybindings)

;; ;;; Company: Text completion package for much more languages: Completion starts as we are writing
(require 'company)
(add-hook 'after-init-hook 'global-company-mode)

;;; Expand-region:
(require 'expand-region)
(global-set-key (kbd "M-m") 'er/expand-region)

(add-hook 'ibuffer-hook
          (lambda ()
            (ibuffer-vc-set-filter-groups-by-vc-root)
            (unless (eq ibuffer-sorting-mode 'alphabetic)
              (ibuffer-do-sort-by-alphabetic))))

(setq ibuffer-formats
      '((mark modified read-only vc-status-mini " "
              (name 18 18 :left :elide)
              " "
              (size 9 -1 :right)
              " "
              (mode 16 16 :left :elide)
              " "
              (vc-status 16 16 :left)
              " "
              filename-and-process)))


(provide 'setup-convenience)
