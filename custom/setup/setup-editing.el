(message "Adding editing setup")

;;; Editing basics
(setq global-mark-ring-max 5000 mark-ring-max 5000 mode-require-final-newline t)

;;; setting terminal encodings
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-language-environment   "UTF-8")
(prefer-coding-system       'utf-8)

;;; tree diffing
(global-set-key (kbd "C-c C-d") 'ztree-diff)
(global-set-key (kbd "C-c C-f") 'ztree-dir)


;;; setting default tab
(delete-selection-mode)
(setq-default tab-width 4)
(setq-default indent-tabs-mode nil)
(global-set-key (kbd "RET") 'newline-and-indent)


;;; setting default killing
(setq kill-ring-max 5000 kill-whole-line t)

;;; setting diff mode display
(add-hook 'diff-mode-hook
          (lambda ()
            (setq-local whitespace-style '(face tabs tab-mark space space-mark trailing newline
                                           indentation::space indentation::tab newline-mark)   )
            (whitespace-mode 1))
          )
;;; Duplicate things
(require 'duplicate-thing)
(global-set-key (kbd "M-c") 'duplicate-thing)

;;; volatile-highlights
(require 'volatile-highlights)
(volatile-highlights-mode t)

;;; smartparens
(require 'smartparens-config)
(setq sp-base-key-bindings 'paredit)
(setq sp-autoskip-closing-pair 'always)
(setq sp-hybrid-kill-entire-symbol nil)
(sp-use-paredit-bindings)
(smartparens-global-mode t)

;;; clean-aindent-mode
(require 'clean-aindent-mode)
(add-hook 'prog-mode-hook 'clean-aindent-mode)

;;; undo-tree
(require 'undo-tree)
(global-undo-tree-mode)

;;; yasnippet
(require 'yasnippet)
(yas-global-mode 1)


(provide 'setup-editing)
