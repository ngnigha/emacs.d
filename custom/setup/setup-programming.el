(message "Adding programming C/C++ support")


;; Adding helm gtags
(require 'setup-helm)
(require 'setup-helm-gtags)

;; Add CEDET SUPPORT
(load-file (concat user-emacs-directory "custom/setup/cedet/cedet-devel-load.el"))
(load-file (concat user-emacs-directory "custom/setup/cedet/contrib/cedet-contrib-load.el")) ;

;; Adding semantic minor mode
(require 'cc-mode)
(require 'semantic)
(global-semanticdb-minor-mode 1)
(global-semantic-idle-scheduler-mode) 1
(semantic-mode 1)

;;; Adding system paths
(semantic-add-system-include "/usr/include/c++")
(semantic-add-system-include "/usr/include/c++/9")
(semantic-add-system-include "/usr/include/c++/10")

(add-hook 'c-mode-common-hook   'hs-minor-mode)

;;; Adding the speedbar
(require 'sr-speedbar)
(setq speedbar-show-unknown-files t)
(setq sr-speedbar-skip-other-window-p t)
(global-set-key (kbd "C-M-o") 'sr-speedbar-open)
(global-set-key (kbd "C-M-p") 'sr-speedbar-toggle)

;;; Adding general completion Company
(require 'company)
(add-hook 'after-init-hook 'global-company-mode)

(setq company-backends (delete 'company-semantic company-backends))
(define-key c-mode-map  [(tab)] 'company-complete)
(define-key c++-mode-map  [(tab)] 'company-complete)


;; Headers completion c/c++
(require 'company-c-headers)
(add-to-list 'company-backends 'company-c-headers)
(add-to-list 'company-c-headers-path-system "/usr/include/c++/5/")

;; ;;; setting default tab size
;; (setq-default c-basic-offset 4)

(require 'diff-hl)
(global-diff-hl-mode)
(add-hook 'dired-mode-hook 'diff-hl-dired-mode)

;;; Adding Magit
(require 'setup-magit)

;;; Checking on the fly
(require 'flycheck-tip)
(add-hook 'after-init-hook #'global-flycheck-mode)

(require 'flycheck-tip)
(define-key prog-mode-map (kbd "C-c C-n") 'flycheck-tip-cycle)
(setq flycheck-display-errors-function 'ignore)
(define-key global-map (kbd "C-0") 'error-tip-cycle-dwim)
(define-key global-map (kbd "C-9") 'error-tip-cycle-dwim-reverse)

(setq c-default-style "linux"
      c-basic-offset 4)

(provide 'setup-programming)
