(message "Setting faces and ui")

;;; turn off bliking cursor
(blink-cursor-mode -1)

;;; Setting frame data
(setq frame-title-format '("" invocation-name " - "
                           (:eval (if (buffer-file-name)
                                      (abbreviate-file-name (buffer-file-name))
                                    "%b"))))

;;; Change font
(setq default-frame-alist '((font . "Inconsolatata-10") (width . 72) n))
(set-face-attribute 'italic nil :family "Inconsolata-Italic")

;;; highlight numbers in program mode
(require 'highlight-numbers)
(add-hook 'prog-mode-hook 'highlight-numbers-mode)

;;; Highlight-symbol: Automatic and manual highlighting of symbols
(require 'highlight-symbol)
(highlight-symbol-nav-mode)
(add-hook 'prog-mode-hook (lambda () (highlight-symbol-mode)))
(add-hook 'org-mode-hook (lambda () (highlight-symbol-mode)))
(setq highlight-symbol-idle-delay 0.2
      highlight-symbol-on-navigation-p t)
(global-set-key [(control shift mouse-1)]
                (lambda (event)
                  (interactive "e")
                  (goto-char (posn-point (event-start event)))
                  (highlight-symbol-at-point)))

(global-set-key (kbd "M-n") 'highlight-symbol-next)
(global-set-key (kbd "M-p") 'highlight-symbol-prev)

(load-theme 'grandshell t)

(provide 'setup-faces-and-ui)
