(message "Setting up helm")

;;; Basic
(require 'helm)
(require 'helm-config)


(global-set-key (kbd "C-c h") 'helm-command-prefix)
(global-unset-key (kbd "C-x c"))

(define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action) ; rebind tab to run persistent action
(define-key helm-map (kbd "C-i") 'helm-execute-persistent-action) ; make TAB work in terminal
(define-key helm-map (kbd "C-z")  'helm-select-action) ; list actions using C-z

(when (executable-find "curl")
  (setq helm-google-suggest-use-curl-p t))

(setq helm-split-window-in-side-p           t ; open helm buffer inside current window, not occupy whole other window
      helm-move-to-line-cycle-in-source     t ; move to end or beginning of source when reaching top or bottom of source.
      helm-ff-search-library-in-sexp        t ; search for library in `require' and `declare-function' sexp.
      helm-scroll-amount                    4 ; scroll 8 lines other window using M-<next>/M-<prior>
      helm-ff-file-name-history-use-recentf t
      helm-echo-input-in-header-line t)

(defun spacemacs-helm-hide-minibuffer-maybe ()
  "Hide minibuffer in Helm session if we use the header line as input field."
  (when (with-helm-buffer helm-echo-input-in-header-line)
    (let ((ov (make-overlay (point-min) (point-max) nil nil t)))
      (overlay-put ov 'window (selected-window))
      (overlay-put ov 'face
                   (let ((bg-color (face-background 'default nil)))
                     `(:background ,bg-color :foreground ,bg-color)))
      (setq-local cursor-type nil))))


(add-hook 'helm-minibuffer-set-up-hook
          'spacemacs-helm-hide-minibuffer-maybe)

(setq helm-autoresize-max-height 40)
(setq helm-autoresize-min-height 20)
(helm-autoresize-mode 1)

(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "M-y") 'helm-show-kill-ring)
(global-set-key (kbd "C-x b") 'helm-mini)
(global-set-key (kbd "C-x C-f") 'helm-find-files)
(global-set-key (kbd "C-x C-l") 'helm-locate)
(global-set-key (kbd "C-c C-a") 'helm-find)
(global-set-key (kbd "C-c h o") 'helm-occur)
(global-set-key (kbd "C-c h g") 'helm-google-suggest)
(global-set-key (kbd "C-c h a") 'helm-man-woman)
(global-set-key (kbd "C-h SPC") 'helm-all-mark-rings)

;; optional fuzzy matching for helm
(setq helm-apropos-fuzzy-match t)
(setq helm-M-x-fuzzy-match t)
(setq helm-lisp-fuzzy-completion t)
(setq helm-locate-fuzzy-match t)
(setq helm-buffers-fuzzy-matching t
      helm-recentf-fuzzy-match    t)

(add-to-list 'helm-sources-using-default-as-input 'helm-source-man-pages)

;;; Definitions in current buffer
(setq-local imenu-create-index-function #'ggtags-build-imenu-index)

(define-key minibuffer-local-map (kbd "C-c C-l") 'helm-minibuffer-history)

(require 'projectile)
(require 'helm-projectile)
(setq projectile-completion-system 'helm)
(projectile-global-mode)
(helm-projectile-on)
(setq projectile-switch-project-action 'helm-projectile)

;;; find references
(global-set-key (kbd "C-c g d") 'helm-gtags-dwim)
(global-set-key (kbd "C-c g s") 'helm-gtags-find-symbol)
(global-set-key (kbd "C-c g r") 'helm-gtags-find-rtag)
(global-set-key (kbd "C-c g a") 'helm-gtags-tags-in-this-function)
(global-set-key (kbd "C-c g f") 'helm-gtags-find-files)
(global-set-key (kbd "C-c g h") 'helm-gtags-show-stack)
;;;(global-set-key (kbd "C-c g h") 'helm-gtags-suggested-key-mapping)

(require 'helm-eshell)
(add-hook 'eshell-mode-hook
          #'(lambda ()
              (define-key eshell-mode-map (kbd "C-c C-l")  'helm-eshell-history))
          )

(helm-mode 1)



(provide 'setup-helm)
