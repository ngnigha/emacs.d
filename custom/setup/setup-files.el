(message "Setting the files module")

(setq large-file-warning-threshold 10000000) ;; size in bytes

;;; Back up

;; No backup for now
(defvar backup-directory "./backup")
(if (not (file-exists-p backup-directory))
    (make-directory backup-directory t))

(setq make-backup-files t        ; backup a file the first time it is saved
      backup-directory-alist `((".*" . ,backup-directory)) ; save backup files in ./backup
      backup-by-copying t     ; copy the current file into backup directory
      version-control t   ; version numbers for backup files
      delete-(or )ld-versions t   ; delete unnecessary versions
      kept-old-versions 6     ; oldest versions to keep when new numbered bkup is made (default: 2)
      kept-new-versions 9 ; newest versions to keep when a new numbered backup is made (default: 2)
      auto-save-default t ; auto-save every buffer that visits a file
      auto-save-timeout 10 ; number of seconds idle time before auto-save (default: 30)
      auto-save-interval 100 ; number of keystrokes between auto-saves (default: 300)
      )

;;; Dired
(setq
    dired-dwim-target t            ; if another Dired buffer is visible in another window, use that directory as target for Rename/Copy
    dired-recursive-copies 'always         ; \"always\" means no asking
    dired-recursive-deletes 'top           ; \"top\" means ask once for top level directory
    dired-listing-switches "-lha"          ; human-readable listing
 )

;; automatically refresh dired buffer on changes
(add-hook 'dired-mode-hook 'auto-revert-mode)

;; if it is not Windows, use the following listing switches
(when (not (eq system-type 'windows-nt))
  (setq dired-listing-switches "-lha --group-directories-first"))

;;; Recentf
(recentf-mode)
(setq recentf-max-menu-items  30
      recentf-max-saved-items 5000
      )

(provide 'setup-files)
