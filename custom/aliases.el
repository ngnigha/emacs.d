(message "Loading aliases")

(defalias 'list-buffers 'ibuffer)
(defalias 'eb 'eval-buffer)
(defalias 'lp 'list-packages)
(defalias 'plp 'package-list-packages)
(defalias 'yes-or-no-p 'y-or-n-p)
(defalias 'ztree-diff 'dd)

;;; Setting F9 as menu bar open
(global-set-key (kbd "<f9>") 'menu-bar-open)

(defun demo()
  ;;; (interactive)
  (message "Hello World" ))
